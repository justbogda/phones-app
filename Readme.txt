Spring boot aplication

---------------- CONTINUT SOFTWARE PORIECT ----------------

Folder-ul phones contine partea de backend realizata in Springoot
Folder-ul phonesfrontend ccontine partea de backend realizata in ReactJS
Baza de date este MySQL din XAMPP


---------------- CERINTE PROIECT ----------------

DONE - 0) Proiect MVC bazat pe un model ce va cuprinde minim 6-7 entitati.
       1) Relatii intre entitati: (DONE) @OneToOne, (NOT DONE) @OneToMany, (NOT DONE) @ManyToOne, (DONE) @ManyToMany
DONE - 2) Se vor implementa toate operatiile CRUD
DONE - 3) Se va testa aplicatia folosid profiluri (prod si dev) si se vor folosi 2 baze de date diferite (prod si dev)
NOT DONE 4) Unit-tests/integration tests
DONE - 5) Se vor valida datele din formulare (partial implementat in React)
DONE - 6) Se vor utiliza log-uri (atat in forntend cat si in backend au fost utilizate)
	 7) (DONE) Optiuni de paginare. (NOT DONE) Sortare de date.
DONE	 8) Spring Security (autentificare jdbc) (implementat doar la nivel de backend +JWT fara frontend, verificata in postman)

Made by Vlaicu Bogdan


 