import React, {useState} from "react";

import {Button, Container, FormControl, Paper} from "@mui/material";
import TextField from "@mui/material/TextField";


const MODAL_STYLE={
    position:'fixed',
    top:'50%',
    left:'50%',
    transform:'translate(-50%,-50%)',
    backgroundColor:'cornflowerblue',
    padding:'10px',
    zIndex:1000,
    borderRadius:'5px'

}
const OVERLAY_STYLE={
    position:'fixed',
    top:0,
    left:0,
    right:0,
    bottom:0,
    backgroundColor:'rgba(0,0,0,0.2)',
    zIndex:1000
}


export default function ModalUpdate({open,onClose, children}){

    if(!open)
        return null;


    return(
        <>

            <div style={OVERLAY_STYLE}>

            </div>
            <div style={MODAL_STYLE}>
                {children}
                <Button
                    onClick={onClose}
                    variant="contained"
                    color="error"
                >Close</Button>

            </div>


        </>

    )
}