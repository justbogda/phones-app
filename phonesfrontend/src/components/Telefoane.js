import * as React from 'react';
import {Container, Paper} from "@mui/material";

import axios from 'axios'
import '../style.css';

import {useEffect, useState} from "react";
import Phones from "./Phones";
import Paginare from "./Paginare";
import ModalUpdate from "./ModalUpdate";


export default function Telefoane() {

    const paperStyle = {padding: '50px 20px 5px 20px', margin:"20px", height:"850px"}

    const [phones, setPhones] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [phonesPerPage, setPhonesPePage] = useState(10);
    const indexLastPhone = currentPage * phonesPerPage;
    const indexFirstPhone = indexLastPhone - phonesPerPage;
    const currentPhone = phones.slice(indexFirstPhone, indexLastPhone);

    useEffect(() => {
        fetch("http://localhost:9000/produse")
            .then(res => res.json())
            .then((result) => {
                setPhones(result);

            })
    })

    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    return (

        <Container>
            <Paper elevation={3} style={paperStyle}>
                <h1>Telefoane</h1>
                <Phones telefoane={currentPhone} loading={loading}/>
                <Paginare randuriPerPage={phonesPerPage} totalRanduri={phones.length} paginate={paginate}/>


            </Paper>
        </Container>


    );
}