import * as React from 'react';
import '../App.css';
import {useState} from "react";
import {Alert, Container, Paper} from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";


export default function Producatori() {

    const paperStyle = {padding: '50px 20px', margin:"20px 20px 30px 20px", height:"300px"}
    const paperStyle2 = {margin: "10px auto"}
    const [producator2, setProducator2] = useState('')


    const handleAdaugaProducator = (e) => {
        e.preventDefault()
        if (producator2 === '') {
            console.log("Numele este gol")
            return (<Alert severity="error">Numele este gol!</Alert>);
        }
        else
            {
                const producator = {producator: producator2}
                console.log(producator)
                fetch("http://localhost:9000/adaugaProducator", {
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify((producator))
                }).then(() => {
                    console.log("Producatorul a fost adaaugat")
                })
            }
        }

        return (

            <Container className="PhoneTextFields">
                <Paper style={paperStyle}>
                    <h1>Adauga producatorul</h1>
                    <form noValidate autoComplete="off">
                        <TextField
                            className="form-control"
                            style={paperStyle2}
                            id="outlined-basic"
                            label="Productor"
                            variant="outlined"
                            fullWidth
                            value={producator2}
                            onChange={(e) => setProducator2(e.target.value)}
                        />
                    </form>
                    <div>
                        <Button
                            id="submitPhone"
                            variant="outlined"
                            onClick={handleAdaugaProducator}>Adauga Producatorul</Button>
                    </div>
                </Paper>

            </Container>


        );
    }