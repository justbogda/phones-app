import React, {useEffect, useState} from 'react';
import {Button, Container, FormControl, InputLabel, MenuItem, Modal, Paper, Select} from "@mui/material";
import '../App.css';
import ModalUpdate from "./ModalUpdate";
import TextField from "@mui/material/TextField";
import {zhTW} from "@mui/material/locale";


function deleteTelefon(id) {
    console.log(id)
    fetch("http://localhost:9000/sterge/" + id, {method: 'DELETE'})
        .then(res => res.json())
        .then((result) => {
            console.log(result)
        });
}


export default function Phones({telefoane, loading}) {

    const [isOpen, setIsOpen] = useState(false)

    const paperStyle = {padding: '50px 20px', margin: "20px", height: "350px"}
    const paperStyle2 = {margin: "10px auto"}
    const [name, setName] = useState('')
    const [anLansare, setAnLansare] = useState('')
    const [pret, setPret] = useState('')
    const [idTelefonUpdate, setIdTelefonUpdate] = useState('')
    const [producatorUpdate, setProducatorUpdate] = useState('')

    function clearFields()
    {
        setAnLansare('')
        setPret('')
        setName('')
    }
    const updatePhone = (e) => {
        e.preventDefault()
        const id = idTelefonUpdate
        const producator = producatorUpdate
        console.log(id, producator)
        const item = {id, producator, name, anLansare, pret}


        console.log(item)
        fetch("http://localhost:9000/update", {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item)

        })
            .then(res => res.json())
            .then((result) => {
                console.log(result)
            });
        clearFields()
        setIsOpen(false)
    }

    return (
        <ul className='list-group mb-4'>
            {telefoane.map((telefon) => (
                <Paper elevation={6}
                       style={{margin: "5px", padding: "10px", textAlign: "left"}} key={telefon.id}>
                    Id:{telefon.id}

                    Producator:{telefon.producator}
                    Name:{telefon.name}
                    An de lansare:{telefon.anLansare}
                    Pret:{telefon.pret}
                    <Button onClick={() => deleteTelefon(telefon.id)}
                            className="butoaneActiuni"
                            variant="contained"
                            color="error">

                        Sterge
                    </Button>
                    <Button
                        className="butoaneActiuni"
                        variant="contained"
                        onClick={() => {
                            setIsOpen(true)
                            setIdTelefonUpdate(telefon.id)
                            setProducatorUpdate(telefon.producator)
                        }}
                        color="warning">
                        Update
                    </Button>
                    <ModalUpdate
                        open={isOpen}
                        onClose={() => setIsOpen(false)}
                    >

                        <Container>
                            <Paper style={paperStyle}>
                                <h1>
                                    Actualizeaza telefonul
                                </h1>
                                <FormControl fullWidth noValidate autoComplete="off">
                                    <TextField
                                        className="form-control"
                                        style={paperStyle2}
                                        label="Nume telefon"
                                        variant="outlined"
                                        fullWidth
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                    />
                                    <TextField
                                        type="number"
                                        className="form-control"
                                        style={paperStyle2}
                                        label="An de lansare"
                                        variant="outlined"
                                        fullWidth
                                        value={anLansare}
                                        onChange={(e) => setAnLansare(e.target.value)}
                                    />
                                    <TextField
                                        type="number"
                                        className="form-control"
                                        label="Pret"
                                        style={paperStyle2}
                                        variant="outlined"
                                        fullWidth
                                        value={pret}
                                        onChange={(e) => setPret(e.target.value)}
                                    />
                                </FormControl>
                            </Paper>
                            <Button
                                variant="contained"
                                color="success"
                                onClick={updatePhone}>
                                Update
                            </Button>
                        </Container>
                    </ModalUpdate>
                </Paper>

            ))}
        </ul>
    );
};


