import * as React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import SmartphoneIcon from '@mui/icons-material/Smartphone';
import FactoryIcon from '@mui/icons-material/Factory';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';

export const SidebarData = [
    {
        title: "Home",
        icon:   <HomeIcon/>,
        link:   "/home"
    },
    {
        title: "Producatori",
        icon:   <FactoryIcon/>,
        link:   "/producatori"
    },
    {
        title: "Telefoane",
        icon:   <SmartphoneIcon/>,
        link:   "/telefoane"
    },
    {
        title: "Login",
        icon:   <LoginIcon/>,
        link:   "/login"
    },
    {
        title: "Logout",
        icon:   <LogoutIcon/>,
        link:   "/logout"
    }
];

