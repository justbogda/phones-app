import * as React from 'react';

import TextField from '@mui/material/TextField';
import {Alert, Container, FormControl, InputLabel, MenuItem, Paper, Select} from "@mui/material";
import '../style.css';
import Button from "@mui/material/Button";
import {useEffect, useState} from "react";
import '../App.css';

export default function PhoneTextFields() {

    const paperStyle = {padding: '50px 20px', margin:"20px", height:"520px"}
    const paperStyle2 = {margin: "10px auto"}
    const [name, setName] = useState('')
    const [producator, setProducator] = useState('')
    const [anLansare, setAnLansare] = useState('')
    const [pret, setPret] = useState('')

    function clearFields()
    {
        setAnLansare('')
        setPret('')
        setName('')
        setProducator('')
    }
    const handleAdaugaTelefon = (e) => {
        e.preventDefault()
        var ok=1;
        if (name === '') {
            ok++;
            console.log("Numele este gol")
            return (<Alert severity="error">Numele este gol!</Alert>);
        } if (producator === '') {
            ok++;
            console.log("Numele este gol")
            return (<Alert severity="error">Numele este gol!</Alert>);
        }  if (anLansare === '') {
            ok++;
            console.log("Campul an de lansare este gol")
            return (<Alert severity="error">Campul cantitate este gol sau are valoarea 0.</Alert>);
        }  if (pret === '' || pret === '0') {
            ok++;
            console.log("Campul pret este gol sau are valoarea 0")
            return (<Alert severity="error">Campul pret este gol sau are valoarea 0.</Alert>);
        }
        if (ok===1)
        {
            const telefon = {producator, name, anLansare, pret}
            console.log(telefon)
            fetch("http://localhost:9000/adaugaprodus", {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify((telefon))
            }).then(() => {
                console.log("Telefonul a fost adaaugat")
            })
        }
        //clearFields()
    }
    const [producatori,setProducatori]= useState([])


    useEffect(()=>{
        fetch("http://localhost:9000/producatori")
            .then(res=>res.json())
            .then((result)=>{
                setProducatori(result);

            })
    })



    return (

        <Container className="PhoneTextFields">
            <Paper style={paperStyle}>
                <h1>Adauga telefonul</h1>
                <FormControl fullWidth noValidate autoComplete="off">
                    <InputLabel id="demo-simple-select-label">Producator</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Producator"
                        style={paperStyle2}

                        variant="outlined"
                        fullWidth
                        value={producator}
                        onChange={(e) => setProducator(e.target.value)}>
                        {producatori.map(prod=>(
                            <MenuItem
                                style={{margin:"10px", padding:"15px",textAlign:"left"}} key={prod.id} value={prod.producator}>
                               {prod.producator}
                            </MenuItem>
                        ))}
                    </Select>
                    <TextField
                        className="form-control"
                        style={paperStyle2}
                        label="Nume telefon"
                        variant="outlined"
                        fullWidth
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <TextField
                        type="number"
                        className="form-control"
                        style={paperStyle2}
                        label="An de lansare"
                        variant="outlined"
                        fullWidth
                        value={anLansare}
                        onChange={(e) => setAnLansare(e.target.value)}
                    />
                    <TextField
                        type="number"
                        className="form-control"
                        style={paperStyle2}
                        label="Pret"
                        variant="outlined"
                        fullWidth
                        value={pret}
                        onChange={(e) => setPret(e.target.value)}
                    />
                </FormControl>
                <div>
                    <Button
                        id="submitPhone"
                        variant="outlined"
                        onClick={handleAdaugaTelefon}>Adauga telefonul</Button>
                </div>
            </Paper>


        </Container>


    );
}