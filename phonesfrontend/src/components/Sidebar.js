import * as React from 'react';
import '../App.css';
import {SidebarData} from './SidebarData';
import {Link} from 'react-router-dom'

export default function Sidebar() {


    return (

        <div className="Sidebar">
            <ul className="SidebarList">

                {SidebarData.map((val,key) => {
                    return <li key={key}
                               id={window.location.pathname=== val.link ? "active" : ""}
                               className="row"
                               > <Link to={val.link}>{" "}
                        <div id="icon">{val.icon}</div>{" "}
                        <div id="title">
                            {val.title}
                        </div>
                    </Link>
                    </li>
                })}

            </ul>

        </div>
    )
}