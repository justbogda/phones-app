package com.application.phones;

import com.application.phones.domain.Role;
import com.application.phones.domain.User;
import com.application.phones.entity.UserInfo;
import com.application.phones.service.UserInfoService;
import com.application.phones.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class PhonesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhonesApplication.class, args);
	}
	@Bean
	PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}
//	@Bean
//	CommandLineRunner run(UserService userService, UserInfoService userInfoService){
//		return args ->{
//			userService.saveRole(new Role( null, "ROLE_USER"));
//			userService.saveRole(new Role( null, "ROLE_MANAGER"));
//			userService.saveRole(new Role( null, "ROLE_ADMIN"));
//			userService.saveRole(new Role( null, "ROLE_SUPER_ADMIN"));
//
//			userService.saveUser(new User(null,"Bogdan", "bogdan","1234",new ArrayList<>()));
//			userService.saveUser(new User(null,"Alex", "alex","1234",new ArrayList<>()));
//			userService.saveUser(new User(null,"Ionut", "ionut","1234",new ArrayList<>()));
//			userService.saveUser(new User(null,"Mihai", "mihai","1234",new ArrayList<>()));
//
//			userService.addRoleToUser("bogdan","ROLE_USER");
//			userService.addRoleToUser("bogdan","ROLE_MANAGER");
//			userService.addRoleToUser("bogdan","ROLE_ADMIN");
//			userService.addRoleToUser("bogdan","ROLE_SUPER_ADMIN");
//		};
//	}

}
