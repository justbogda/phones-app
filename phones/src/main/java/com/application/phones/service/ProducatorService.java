package com.application.phones.service;

import com.application.phones.entity.Producator;
import com.application.phones.entity.Telefon;
import com.application.phones.repository.ProducatorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class ProducatorService {
    @Autowired
    private ProducatorRepository repository;

    public Producator saveProducator(Producator producator){
        return repository.save(producator);
    }

    public List <Producator> getProducatori(){
        return repository.findAll();
    }
    public Producator getProducatorById(int id){
        return repository.findById(id).orElse(null);
    }
    public Producator getProducatorByProducator(String producator){
        return repository.findByProducator(producator);
    }
    public String deleteProducator(int id){
        repository.deleteById(id);
        return "Producator eliminat !!" + id;
    }
}
