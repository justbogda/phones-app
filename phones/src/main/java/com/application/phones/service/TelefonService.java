package com.application.phones.service;

import com.application.phones.entity.Telefon;
import com.application.phones.repository.TelefonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class TelefonService {
    @Autowired
    private TelefonRepository repository;

    public Telefon saveTelefon(Telefon telefon){
        return repository.save(telefon);
    }
    public List<Telefon> saveTelefoane(List<Telefon> Telefoane){
        return repository.saveAll(Telefoane);
    }
    public List<Telefon> getTelefoane(){
        return repository.findAll();
    }
    public Telefon getTelefonById(int id){
        return repository.findById(id).orElse(null);
    }
    public Telefon getTelefonByName(String name){
        return repository.findByName(name);
    }
    public Telefon getTelefonByProducator(String producator){
        return repository.findByProducator(producator);
    }
    public String deleteTelefon(int id){
        repository.deleteById(id);
        return "produs eliminat !!" + id;
    }
    public Telefon updateTelefon (Telefon telefon){
        Telefon existingTelefon =repository.findById(telefon.getId()).orElse(null);
        existingTelefon.setName(telefon.getName());
        existingTelefon.setProducator(telefon.getProducator());
        existingTelefon.setAnLansare(telefon.getAnLansare());
        existingTelefon.setPret(telefon.getPret());
        return repository.save(existingTelefon);
    }
}
