package com.application.phones.service;


import com.application.phones.domain.User;
import com.application.phones.entity.UserInfo;
import com.application.phones.repository.UserInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserInfoService {

    @Autowired
    private UserInfoRepository repository;

    public UserInfo saveUserInfo(UserInfo userInfo){
        return repository.save(userInfo);
    }
    public UserInfo getUserInfoByUserId(Long id){
        return repository.findByUserId(id);
    }
}
