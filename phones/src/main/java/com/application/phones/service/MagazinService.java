package com.application.phones.service;

import com.application.phones.entity.Magazin;
import com.application.phones.repository.MagazinRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class MagazinService {

    @Autowired
    private MagazinRepository repository;

    public Magazin saveMagazin(Magazin magazin){
        return repository.save(magazin);
    }
    public List<Magazin> getMagazine(){
        return repository.findAll();
    }


}
