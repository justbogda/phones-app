package com.application.phones.repository;

import com.application.phones.entity.Producator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProducatorRepository extends JpaRepository<Producator, Integer> {
    Producator findByProducator(String producator);
}
