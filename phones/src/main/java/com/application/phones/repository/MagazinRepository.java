package com.application.phones.repository;

import com.application.phones.entity.Magazin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MagazinRepository extends JpaRepository<Magazin, Integer> {
    Magazin findByMagazin(String magazin);
}
