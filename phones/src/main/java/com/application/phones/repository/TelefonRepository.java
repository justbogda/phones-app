package com.application.phones.repository;

import com.application.phones.entity.Telefon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TelefonRepository extends JpaRepository<Telefon, Integer> {

    Telefon findByName(String name);
    Telefon findByProducator(String name);
}
