package com.application.phones.controller;

import com.application.phones.entity.UserInfo;
import com.application.phones.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UserInfoController {

    @Autowired
    private UserInfoService service;
    @PostMapping("/userinfo/adauga")
    public String adaugaUserInfo(@RequestBody UserInfo userInfo){
        service.saveUserInfo(userInfo);
        return "Am adaugat detalii user!";
    }
    @GetMapping("/userinfo/{id_user}")
    public UserInfo getUserInfoByUserId(@PathVariable Long id_user){
        return service.getUserInfoByUserId(id_user);
    }
}
