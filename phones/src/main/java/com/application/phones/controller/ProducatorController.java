package com.application.phones.controller;

import com.application.phones.entity.Producator;
import com.application.phones.service.ProducatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ProducatorController {

    @Autowired
    private ProducatorService service;

    @PostMapping("/adaugaProducator")
    public String adaugaProducator(@RequestBody Producator producator){
        service.saveProducator(producator);
        return "Un producator a fost adaugat";
    }
    @GetMapping("/producatori")
    public List<Producator> findAllProducatori(){
        return service.getProducatori();
    }
    @GetMapping("/poducator/{id}")
    public Producator findProducatorById(@PathVariable int id){

        return service.getProducatorById(id);
    }
    @GetMapping("/poducator/{nume}")
    public Producator findProducatorByName(@PathVariable String producator){
        return service.getProducatorByProducator(producator);
    }
    @DeleteMapping("/stergeProducator/{id}")
    public String deleteProducator(@PathVariable int id)
    {
        return service.deleteProducator(id);
    }


}
