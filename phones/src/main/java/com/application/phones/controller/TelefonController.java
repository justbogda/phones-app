package com.application.phones.controller;


import com.application.phones.entity.Telefon;
import com.application.phones.service.TelefonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class TelefonController {

    @Autowired
    private TelefonService service;


    @PostMapping("/adaugaprodus")
    public String addTelefon(@RequestBody Telefon telefon) {
        service.saveTelefon(telefon);
        return "Un telefon nou a fost adaugat";
    }

    @PostMapping("/adaugaproduse")
    public List<Telefon> addTelefoane(@RequestBody List<Telefon> Telefoane) {
        return service.saveTelefoane(Telefoane);
    }
    @GetMapping ("/produse")
    public List<Telefon> findAllTelefoane(){
        return service.getTelefoane();
    }
    @GetMapping("/produs/{id}")
    public Telefon findTelefonById(@PathVariable int id){
        return service.getTelefonById(id);
    }
    @GetMapping("/produs/{nume}")
    public Telefon findTelefonByName(@PathVariable String name){
        return service.getTelefonByName(name);
    }
    @GetMapping("/produs/{producator}")
    public Telefon findTelefonByProducator(@PathVariable String producator){
        return service.getTelefonByProducator(producator);
    }
    @PutMapping("/update")
    public Telefon UpdateTelefon(@RequestBody Telefon telefon){
        return service.updateTelefon(telefon);
    }

    @DeleteMapping("/sterge/{id}")
    public String deleteTelefon(@PathVariable int id)
    {
        return service.deleteTelefon(id);
    }
}
