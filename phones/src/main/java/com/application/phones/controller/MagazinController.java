package com.application.phones.controller;

import com.application.phones.entity.Magazin;
import com.application.phones.service.MagazinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class MagazinController {


    @Autowired
    private MagazinService service;

    @PostMapping("/magazin/salveaza")
    public String adaugaMagazin(@RequestBody Magazin magazin){
        service.saveMagazin(magazin);
        return "Un magazin a fost adaugat!";
    }

    @GetMapping("/magazine/all")
    public List<Magazin> findAllMagazine(){
        return service.getMagazine();
    }
}
