package com.application.phones.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Magazin")
public class Magazin {

    @Id
    @GeneratedValue
    private int id;
    private String magazin;
    private String email;
    private String judet;
    private String localitate;
    private String strada;
    private String nrAdresa;
    private String nrTelefon;
}
