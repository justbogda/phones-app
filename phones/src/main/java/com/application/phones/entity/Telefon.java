package com.application.phones.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name = "Telefoane")
public class Telefon {
    @Id
    @GeneratedValue
    private int id;
    private String producator;
    private String name;
    private int anLansare;
    private double pret;
}
